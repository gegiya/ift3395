import keras
from keras.datasets import mnist
from keras.models import Sequential
from keras.layers import Dense, Dropout, Flatten
from keras.layers import Conv2D, MaxPooling2D
from keras import backend as K
from keras.utils import plot_model

import numpy as np




model = Sequential()
model.add(Dense(units=64, activation='relu', input_shape=(2,)))
model.add(Dense(units=2, activation='softmax'))
model.compile(loss=keras.losses.sparse_categorical_crossentropy,
              optimizer=keras.optimizers.SGD(lr=0.01, momentum=0.9, nesterov=True),
              metrics=['accuracy'])

#plot_model(model, to_file='model_mlp.png')
data = np.loadtxt(open('2moons.txt', 'r'))
np.random.shuffle(data)
train_data, test_data = np.vsplit(data, 2)
x_train = train_data[:, [0, 1]]
y_train = train_data[:, 2]

x_test = test_data[:, [0, 1]]
y_test = test_data[:, 2]


model.fit(x_train, y_train, epochs=100, batch_size=10, validation_data=(x_test, y_test))

score = model.evaluate(x_test, y_test, verbose=0)
print('Test loss:', score[0])
print('Test accuracy:', score[1])


