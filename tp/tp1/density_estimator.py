import numpy as np


class GaussianDiagonal:

    def __init__(self):
        self.mean = np.array([])
        self.sigma_square = np.array([])
        self.normalisation_constant = 0

    def train(self, train_data):
        """
        Calculate actual mean and covariance matrix for the given distribution
        :param train_data: n - dimensional np.array, where the each row is a sample vector
        :return:
        """
        num_examples = train_data.shape[0]
        num_dimensions = train_data.shape[1] if len(train_data.shape) > 1 else 1
        self.mean = np.mean(train_data, axis=0)
        self.sigma_square = np.sum((train_data - self.mean) ** 2, axis=0) / num_examples
        self.normalisation_constant = - num_dimensions * np.log(2 * np.pi) / 2.0 - num_dimensions * np.log(np.prod(self.sigma_square)) / 2.0

    def compute_predictions(self, test_data):
        """
        Calculate the probability log for the each sample in the provided test data
        :param test_data: n - dimensional np.array, where the each row is a sample vector
        :return:
        """
        if len(test_data.shape) > 1:
            return self.normalisation_constant - np.sum((test_data - self.mean) ** 2 / (2 * self.sigma_square), axis=1)
        else:
            return self.normalisation_constant - (test_data - self.mean) ** 2 / (2 * self.sigma_square)


class ParzenIsotropicDensityEstimator:

    def __init__(self, sigma):
        self.train_data = None
        self.sigma = sigma
        self.num_dimensions = 0
        self.num_samples = 0

    def kernel_function(self, x, mean):
        """
        Gaussian isotropic density function
        :param x: sample vector as np.array
        :param mean: mean point as np.array
        :return:
        """
        part1 = 1 / ((self.sigma ** self.num_dimensions) * ((2 * np.pi) ** (self.num_dimensions / 2.0)))
        part2 = (-1 / 2.0) * np.sum((x - mean) ** 2.0) / (self.sigma ** 2.0)
        return float(part1 * np.exp(part2))

    def train(self, train_data):
        """
        Store the train data
        :param train_data:
        :return:
        """
        self.train_data = train_data
        self.num_samples = train_data.shape[0]
        self.num_dimensions = train_data.shape[1] if len(train_data.shape) > 1 else 1

    def avg_density(self, x):
        """
        This is a soft Parzen window method, all the train data will be used to compute the prediction
        :param x:
        :return:
        """
        sum_all = 0
        for row in self.train_data:
            sum_all += self.kernel_function(x, row)
        return float(sum_all) / self.num_samples

    def compute_predictions(self, test_data):
        """
        Compute the log of the probability density function for the points in the given test data
        :param test_data:
        :return:
        """
        return np.log(np.array([self.avg_density(x) for x in test_data]))
