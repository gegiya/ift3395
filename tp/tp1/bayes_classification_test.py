import numpy as np

from bayes import BayesClassificator
from density_estimator import GaussianDiagonal,ParzenIsotropicDensityEstimator
from matplotlib import pyplot as plt
import utilitaires


def data_set_2():
    iris = np.loadtxt('iris.txt')[:, [0, 1, 4]]
    np.random.seed(123)
    indices1 = np.arange(0, 50)
    indices2 = np.arange(50, 100)
    indices3 = np.arange(100, 150)

    np.random.shuffle(indices1)
    np.random.shuffle(indices2)
    np.random.shuffle(indices3)

    iris_train1 = iris[indices1[:35]]
    iris_test1 = iris[indices1[35:]]
    iris_train2 = iris[indices2[:35]]
    iris_test2 = iris[indices2[35:]]
    iris_train3 = iris[indices3[:35]]
    iris_test3 = iris[indices3[35:]]

    iris_train = np.concatenate([iris_train1, iris_train2, iris_train3])
    iris_test = np.concatenate([iris_test1, iris_test2, iris_test3])
    return iris_train, iris_test


def data_set_4():
    iris = np.loadtxt('iris.txt')[:, [0, 1, 2, 3, 4]]
    np.random.seed(123)
    indices1 = np.arange(0, 50)
    indices2 = np.arange(50, 100)
    indices3 = np.arange(100, 150)

    np.random.shuffle(indices1)
    np.random.shuffle(indices2)
    np.random.shuffle(indices3)

    iris_train1 = iris[indices1[:35]]
    iris_test1 = iris[indices1[35:]]
    iris_train2 = iris[indices2[:35]]
    iris_test2 = iris[indices2[35:]]
    iris_train3 = iris[indices3[:35]]
    iris_test3 = iris[indices3[35:]]

    iris_train = np.concatenate([iris_train1, iris_train2, iris_train3])
    iris_test = np.concatenate([iris_test1, iris_test2, iris_test3])
    return iris_train, iris_test


def test_diagonal_2():
    classifier = BayesClassificator(GaussianDiagonal)
    iris_train, iris_test  = data_set_2()
    classifier.train(iris_train)

    test_cols = [0, 1]
    # on peut maintenant calculer les logs-probabilites selon nos modeles
    log_prob_train = classifier.compute_predictions(iris_train[:, test_cols])
    log_prob_test = classifier.compute_predictions(iris_test[:, test_cols])

    # il reste maintenant a calculer le maximum par classe pour la classification
    classes_pred_train = log_prob_train.argmax(axis=1) + 1
    classes_pred_test = log_prob_test.argmax(axis=1) + 1

    print "Taux d'erreur (entrainement) %.2f%%" % ((1 - (classes_pred_train == iris_train[:, -1]).mean()) * 100.0)
    print "Taux d'erreur (test) %.2f%%" % ((1 - (classes_pred_test == iris_test[:, -1]).mean()) * 100.0)
    utilitaires.gridplot(classifier,
                         iris_train[:, test_cols + [-1]],
                         iris_test[:, test_cols + [-1]],
                         n_points=50)


def test_gaussian_diagonal_4():
    classifier = BayesClassificator(GaussianDiagonal)
    iris_train, iris_test = data_set_4()
    classifier.train(iris_train)

    test_cols = [0, 1, 2, 3]
    # on peut maintenant calculer les logs-probabilites selon nos modeles
    log_prob_train = classifier.compute_predictions(iris_train[:, test_cols])
    log_prob_test = classifier.compute_predictions(iris_test[:, test_cols])

    # il reste maintenant a calculer le maximum par classe pour la classification
    classes_pred_train = log_prob_train.argmax(axis=1) + 1
    classes_pred_test = log_prob_test.argmax(axis=1) + 1

    print "Taux d'erreur (entrainement) %.2f%%" % ((1 - (classes_pred_train == iris_train[:, -1]).mean()) * 100.0)
    print "Taux d'erreur (test) %.2f%%" % ((1 - (classes_pred_test == iris_test[:, -1]).mean()) * 100.0)


def compute_parzen_2(h):
    classifier = BayesClassificator(ParzenIsotropicDensityEstimator, sigma=h)
    iris_train, iris_test = data_set_2()
    classifier.train(iris_train)

    # on peut maintenant calculer les logs-probabilites selon nos modeles
    test_cols = [0, 1]
    # on peut maintenant calculer les logs-probabilites selon nos modeles
    log_prob_train = classifier.compute_predictions(iris_train[:, test_cols])
    log_prob_test = classifier.compute_predictions(iris_test[:, test_cols])

    # il reste maintenant a calculer le maximum par classe pour la classification
    classes_pred_train = log_prob_train.argmax(axis=1) + 1
    classes_pred_test = log_prob_test.argmax(axis=1) + 1

    print "Taux d'erreur (entrainement) %.2f%%" % ((1 - (classes_pred_train == iris_train[:, -1]).mean()) * 100.0)
    print "Taux d'erreur (test) %.2f%%" % ((1 - (classes_pred_test == iris_test[:, -1]).mean()) * 100.0)
    utilitaires.gridplot(classifier,
                         iris_train[:, test_cols + [-1]],
                         iris_test[:, test_cols + [-1]],
                         n_points=50)


def test_parzen_2():
    compute_parzen_2(0.05)
    compute_parzen_2(2)
    compute_parzen_2(0.2)


def test_parzen_error_rate():
    parzen_error_rate([0, 1, 4], data_set_2)
    parzen_error_rate([0, 1, 2, 3, 4], data_set_4)


def parzen_error_rate(columns, data_set):
    sigmas = np.arange(0.0001, 1, 0.01)
    iris_train, iris_test = data_set()
    errors_test = []
    errors_train = []
    for s in sigmas:
        error_train, error_test = calculate_parzen_error(iris_train, iris_test, s, columns[:-1])
        errors_test.append(error_test)
        errors_train.append(error_train)

    plt.figure()
    plt.title('Parzen window model error rate for %s attributes' % (len(columns) - 1))
    plt.grid(True)
    plt.xlabel("$\sigma$)")
    plt.ylabel(r"l($\sigma$)")
    train_, = plt.plot(sigmas, errors_train, c="b", label="train error")
    test_, = plt.plot(sigmas, errors_test, c="r", label="test error")
    plt.legend(handles=[train_, test_])
    opt_sigma = sigmas[np.argmin(errors_test)]
    plt.show()
    print "Taux d'erreur (entrainement) min %.2f%%" % (min(errors_train) * 100.0)
    print "Taux d'erreur (test) min %.2f%%" % (min(errors_test) * 100.0)
    print "Sigma optimal %.2f" % opt_sigma


def calculate_parzen_error(iris_train, iris_test, sigma, train_cols):
    classifier = BayesClassificator(ParzenIsotropicDensityEstimator, sigma=sigma)

    # On cree notre classifieur avec notre liste de modeles gaussien et nos priors
    classifier.train(iris_train)

    # on peut maintenant calculer les logs-probabilites selon nos modeles
    # on peut maintenant calculer les logs-probabilites selon nos modeles
    log_prob_train = classifier.compute_predictions(iris_train[:, train_cols])
    log_prob_test = classifier.compute_predictions(iris_test[:, train_cols])

    # il reste maintenant a calculer le maximum par classe pour la classification
    classes_pred_train = log_prob_train.argmax(axis=1) + 1
    classes_pred_test = log_prob_test.argmax(axis=1) + 1
    error_train = 1 - (classes_pred_train == iris_train[:, -1]).mean()
    error_test = 1 - (classes_pred_test == iris_test[:, -1]).mean()
    return error_train, error_test
