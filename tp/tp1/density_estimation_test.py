import numpy as np
from matplotlib import pyplot as plt
from density_estimator import GaussianDiagonal, ParzenIsotropicDensityEstimator


def test_1d():
    # prepare data
    iris = np.loadtxt("iris.txt")
    data_set = iris[iris[:, 4] == 2][:, 0]

    # draw
    plt.figure()
    plt.title('Density 1d')
    plt.grid(True)
    plt.xlabel("x")
    plt.ylabel("log[p(x)]")

    plot_points1d(data_set)
    legend = [plot_gauss1d(data_set)]

    for h, color in [(0.05, "r"), (2, "y"), (0.2, "g")]:
        legend.append(plot_parzen1d(data_set, h, color))
    plt.legend(handles=legend)
    plt.show()


def plot_points1d(data_set):
    plt.plot(data_set, np.zeros(len(data_set)), 'bo')


def plot_gauss1d(data_set):
    # train
    estimator = GaussianDiagonal()
    estimator.train(data_set)

    # compute predictions
    (min_x1, max_x1) = (min(data_set), max(data_set))
    xgrid = np.linspace(min_x1, max_x1, num=100)
    y = estimator.compute_predictions(xgrid)

    # draw
    gauss, = plt.plot(xgrid, y, c="b", label="gaussian")
    return gauss


def plot_parzen1d(data_set, h, color):
    # train
    estimator = ParzenIsotropicDensityEstimator(h)
    estimator.train(data_set)

    # compute predictions
    (min_x1, max_x1) = (min(data_set), max(data_set))
    xgrid = np.linspace(min_x1, max_x1, num=100)
    y = estimator.compute_predictions(xgrid)

    # draw
    parzen, = plt.plot(xgrid, y, c=color, label=r"parzen $\sigma$=%s" % h)
    return parzen


def test_2d():
    iris = np.loadtxt("iris.txt")
    data_set = iris[iris[:, 4] == 2][:, [0, 1]]
    plot_gaussian2d(data_set)
    plot_parzen2d(data_set, 0.05)
    plot_parzen2d(data_set, 2)
    plot_parzen2d(data_set, 0.2)
    plt.show()


def plot_parzen2d(data_set, h):
    # train
    estimator = ParzenIsotropicDensityEstimator(h)
    estimator.train(data_set)

    # compute predictions
    (min_x0, max_x0) = (min(data_set[:, 0]), max(data_set[:, 0]))
    xgrid = np.linspace(min_x0, max_x0, num=100)
    (min_x1, max_x1) = (min(data_set[:, 1]), max(data_set[:, 1]))
    ygrid = np.linspace(min_x1, max_x1, num=100)
    # generate a cartesian product of 2 lists
    thegrid = np.array(np.meshgrid(xgrid, ygrid)).T.reshape(-1, 2)
    predictions = estimator.compute_predictions(thegrid)

    # draw
    plt.figure()
    plt.title(r"Estimateur de Parzen avec $\sigma$=%s" % h)
    plt.grid(True)
    plt.xlabel("x1")
    plt.ylabel("x2")

    # draw points
    x0 = data_set[:, 0]
    x1 = data_set[:, 1]
    plt.plot(x0, x1, 'bo')

    # draw contours
    X, Y = np.meshgrid(xgrid, ygrid)
    Z = predictions.reshape(100, 100)
    cs = plt.contour(X, Y, Z)
    plt.clabel(cs, inline=1, fontsize=10)


def plot_gaussian2d(data_set):
    # train
    estimator = GaussianDiagonal()
    estimator.train(data_set)

    # compute predictions
    (min_x0, max_x0) = (min(data_set[:, 0]), max(data_set[:, 0]))
    xgrid = np.linspace(min_x0, max_x0, num=100)
    (min_x1, max_x1) = (min(data_set[:, 1]), max(data_set[:, 1]))
    ygrid = np.linspace(min_x1, max_x1, num=100)
    # generate a cartesian product of 2 lists
    thegrid = np.array(np.meshgrid(xgrid, ygrid)).T.reshape(-1, 2)
    predictions = estimator.compute_predictions(thegrid)

    # draw
    plt.figure()
    plt.title("Estimateur Gaussien Diagonal")
    plt.grid(True)
    plt.xlabel("x1")
    plt.ylabel("x2")

    # draw points
    x0 = data_set[:, 0]
    x1 = data_set[:, 1]
    plt.plot(x0, x1, 'bo')

    # draw contours
    X, Y = np.meshgrid(xgrid, ygrid)
    Z = predictions.reshape(100, 100)
    cs = plt.contour(X, Y, Z)
    plt.clabel(cs, inline=1, fontsize=10)


def main():
    test_1d()
    test_2d()

if __name__ == "__main__":
    main()
