import numpy as np


class BayesClassModel:

    def __init__(self, label, model, prior):
        self.label = label
        self.model = model
        self.prior = prior

    def compute_predictions(self, test_data):
        model_prediction = self.model.compute_predictions(test_data) + np.log(self.prior)
        return model_prediction


class BayesClassificator:

    def __init__(self, model, **kwargs):
        self.data_set = np.array([])
        self.classes = []
        self.model = model
        self.model_args = kwargs

    def train(self, data_set):
        """
        extract classes and calculate priors for each class
        :param data_set:
        :return:
        """
        label_set = set(data_set[:, -1])  # last column
        for label in label_set:
            model = self.model(**self.model_args)
            # select entries with for the current class
            train_set = data_set[data_set[:, -1] == label][:, :-1]  # all columns except the last one
            model.train(train_set)
            prior = len(train_set) / float(len(data_set))
            self.classes.append(BayesClassModel(label, model, prior))

    def compute_predictions(self, test_data):
        predictions = np.empty((test_data.shape[0], len(self.classes)))
        for idx, _class in enumerate(self.classes):
            predictions[:, idx] = _class.compute_predictions(test_data)
        return predictions
